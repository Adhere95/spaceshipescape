﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawns the asteroids in the game.
/// </summary>
public class AsteroidSpawner : MonoBehaviour
{
    private float minimumRange = -7.5f;
    private float maximumRange = 7.5f;

    private float timeMinimum = 0.5f;
    private float timeMaximum = 1.0f;

    private float yPosition = 6.0f;

    public GameObject[] prefabs = new GameObject[5];

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawning());
    }

    private IEnumerator Spawning()
    {
        while(true)
        {
            float xPosition = Random.Range(minimumRange, maximumRange);
            Vector2 position = new Vector2(xPosition, yPosition);
            SpawnAsteroid(position);
            yield return new WaitForSeconds(Random.Range(timeMinimum, timeMaximum));
        }
    }

    /// <summary>
    /// Spawn an asteroid in the position.
    /// </summary>
    /// <param name="position">Position where the asteroid is spawned.</param>
    public void SpawnAsteroid(Vector2 position)
    {
        int index = Random.Range(0, prefabs.Length);
        Instantiate(prefabs[index], position, Quaternion.identity);
    }

}
