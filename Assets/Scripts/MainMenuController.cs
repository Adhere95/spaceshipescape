﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

/// <summary>
/// The class controls the main menu of the game.
/// </summary>
public class MainMenuController : MonoBehaviour
{
    private static MainMenuController _instance;

    /// <summary>
    /// Singleton instance of the class.
    /// </summary>
    public static MainMenuController Instance { get { return _instance; } }

    /// <summary>
    /// Text displaying player's highscore.
    /// </summary>
    public TextMeshProUGUI highScoreText;
    /// <summary>
    /// Canvas containing the main menu.
    /// </summary>
    public GameObject mainMenuCanvas;
    /// <summary>
    /// Canvas containing the instructions menu.
    /// </summary>
    public GameObject instructionsCanvas;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ShowHighScore();
    }

    private void ShowHighScore()
    {
        highScoreText.text = "Highscore: " + PlayerPrefs.GetInt("HighScore", 0);
    }

    /// <summary>
    /// Starts the game.
    /// </summary>
    public void StartGameOnClick()
    {
        SceneManager.LoadScene("Level");
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// Show instructions canvas.
    /// </summary>
    public void ShowInstructions()
    {
        mainMenuCanvas.SetActive(false);
        instructionsCanvas.SetActive(true);
    }

    /// <summary>
    /// Returns back to main menu.
    /// </summary>
    public void BackToMenu()
    {
        instructionsCanvas.SetActive(false);
        mainMenuCanvas.SetActive(true);
    }

    /// <summary>
    /// Resets the player's highscore.
    /// </summary>
    public void ResetHighScore()
    {
        PlayerPrefs.SetInt("HighScore", 0);
        ShowHighScore();
    }

}
