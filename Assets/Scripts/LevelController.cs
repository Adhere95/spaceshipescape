﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// The class controls the logic of the level.
/// </summary>
public class LevelController : MonoBehaviour
{

    /// <summary>
    /// Loads the main menu.
    /// </summary>
    public void LoadMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
