﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles an individual asteroid in the game.
/// </summary>
public class AsteroidHandler : MonoBehaviour
{
    private float timeLimit = 2.5f;
    private float timeElapsed = 0.0f;
    private float movingSpeed = 5.0f;

    // Update is called once per frame
    void Update()
    {
        MoveAsteroid();
        CalculateTime();
    }

    private void MoveAsteroid()
    {
        Vector2 position = this.transform.position;
        this.transform.position = new Vector2(position.x, position.y - movingSpeed * Time.deltaTime);
    }

    private void CalculateTime()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= timeLimit)
        {
            ScoreController.Instance.IncrementScore();
            Destroy(this.gameObject);
        }
    }

}
