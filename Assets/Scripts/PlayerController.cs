﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class controlls the player logic.
/// </summary>
public class PlayerController : MonoBehaviour
{
    private static PlayerController _instance;

    /// <summary>
    /// Singleton instance of the class.
    /// </summary>
    public static PlayerController Instance { get { return _instance; } }
    /// <summary>
    /// Prefab for explosion in the game.
    /// </summary>
    public GameObject explosionPrefab;
    /// <summary>
    /// Sprites displaying current lives of the player.
    /// </summary>
    public GameObject[] livesSprites = new GameObject[5];
    /// <summary>
    /// Asteroid spawner.
    /// </summary>
    public AsteroidSpawner asteroidSpawner;
    /// <summary>
    /// Audio source for the explosions.
    /// </summary>
    public AudioSource explosionSound;

    private bool movingLeft = false;
    private bool movingRight = false;
    private float movementSpeed = 5.0f;
    private int lives = 5;
    private string asteroidTag = "Asteroid";

    private float idleLimit = 2.0f;
    private float asteroidOffset = 10.0f;
    private float minimumDifference = 1.0f;
    private Vector2 previousPosition;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CheckIdle());
    }

    // Update is called once per frame
    void Update()
    {
        CheckMovement();
    }

    private void CheckMovement()
    {
        if (movingLeft == true)
        {
            HandleMovement(-1);
        }
        else if (movingRight == true)
        {
            HandleMovement(1);
        }
    }

    private IEnumerator CheckIdle()
    {
        while (true)
        {
            Vector2 position = this.transform.position;
            previousPosition = new Vector2(position.x, position.y);

            yield return new WaitForSeconds(idleLimit);

            position = this.transform.position;
            float difference = Mathf.Abs(position.x - previousPosition.x);
            if (difference < minimumDifference)
            {
                Vector2 spawnPosition = new Vector2(position.x, position.y + asteroidOffset);
                asteroidSpawner.SpawnAsteroid(spawnPosition);
            }
        }
    }

    private void HandleMovement(int sign)
    {
        Vector2 position = transform.position;
        Vector2 newPosition = new Vector2(position.x + movementSpeed * Time.deltaTime * sign, position.y);
        transform.position = newPosition;
    }

    /// <summary>
    /// Action called when the left button on the screen is pressed.
    /// </summary>
    public void LeftButtonPressed()
    {
        if (movingRight == false)
        {
            movingLeft = true;
        }
    }

    /// <summary>
    /// Action called when the right button on the screen is pressed.
    /// </summary>
    public void RightButtonPressed()
    {
        if (movingLeft == false)
        {
            movingRight = true;
        }
    }

    /// <summary>
    /// Action called when the left button on the screen is released.
    /// </summary>
    public void LeftButtonReleased()
    {
        movingLeft = false;
    }

    /// <summary>
    /// Action called when the right button on the screen is released.
    /// </summary>
    public void RightButtonReleased()
    {
        movingRight = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag.Equals(asteroidTag))
        {
            HandleAsteroidCollision(collision);
        }
    }

    private void HandleAsteroidCollision(Collider2D collision)
    {
        Instantiate(explosionPrefab, collision.transform.position, Quaternion.identity);
        Destroy(collision.gameObject);

        explosionSound.Play();

        lives--;
        DecrementLives();
        if (lives == 0)
        {
            Instantiate(explosionPrefab, this.transform.position, Quaternion.identity);
            ScoreController.Instance.Playing = false;
            ScoreController.Instance.ShowScorePanel();
            Destroy(asteroidSpawner);
            Destroy(this.gameObject);

        }

    }

    private void DecrementLives()
    {
        livesSprites[lives].SetActive(false);
    }

}
