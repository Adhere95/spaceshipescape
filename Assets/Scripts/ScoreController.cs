﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// The class controls score of the player.
/// </summary>
public class ScoreController : MonoBehaviour
{
    private static ScoreController _instance;

    /// <summary>
    /// Singleton instance of the class.
    /// </summary>
    public static ScoreController Instance { get { return _instance; } }

    /// <summary>
    /// Is the player playing the game?
    /// </summary>
    public bool Playing { get; set; }
    /// <summary>
    /// Shows current score of the player.
    /// </summary>
    public TextMeshProUGUI scoreBoard;
    /// <summary>
    /// Score panel.
    /// </summary>
    public GameObject scorePanel;
    /// <summary>
    /// Shows final score of the player.
    /// </summary>
    public TextMeshProUGUI finalScore;

    private int score = 0;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Playing = true;
        scorePanel.SetActive(false);
    }

    /// <summary>
    /// Increments player score by one point.
    /// </summary>
    public void IncrementScore()
    {
        if (Playing == true)
        {
            score++;
            scoreBoard.text = "SCORE: " + score;
        }
    }

    /// <summary>
    /// Shows the score panel.
    /// </summary>
    public void ShowScorePanel ()
    {
        scorePanel.SetActive(true);
        finalScore.text = "Your score is: " + score;
        SaveHighScore();
    }

    /// <summary>
    /// Saves the highscore of the player.
    /// </summary>
    private void SaveHighScore()
    {
        int currentHighScore = PlayerPrefs.GetInt("HighScore", 0);
        if (score > currentHighScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
    }

}
